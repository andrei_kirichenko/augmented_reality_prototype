#include "ogl_camera.h"
#include "arucosrc.h" // Markers #3 and #45

Backgound a;
ArucoSrc b;
Planet c;
Cube d;

int main()
{
	while (!glfwWindowShouldClose(a.getWindow()))
	{
		a.draw();

		// Draw cube
		//b.findArucoMarkersRender3D(a.getImage(), d);

		// Draw planet model
		b.findArucoMarkersRender3D(a.getImage(), c);

		a.swapBuffers();
	}

	return 0;
}

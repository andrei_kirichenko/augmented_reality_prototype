#pragma once
#include "calibrator.h"
#include "cube.h"
#include "planet.h"

class ArucoSrc : protected Calibrator
{
public:
	ArucoSrc()
	{
		for (int i{}; i < 50; ++i) rotation_ogl.push_back(cv::Mat::zeros(4, 4, CV_64F));
		for (int i{}; i < 50; ++i) translation_ogl.push_back(cv::Mat::zeros(4, 1, CV_64F));
		for (int i{}; i < 50; ++i) model_view_matx.push_back(cv::Mat::eye(4, 4, CV_64F));
		for (int i{}; i < 50; ++i) projection_matx.push_back(cv::Mat::zeros(4, 4, CV_64F));

		markerDict = cv::aruco::getPredefinedDictionary(cv::aruco::PREDEFINED_DICTIONARY_NAME::DICT_4X4_50);
		FLAG = 0;
	}

	void findArucoMarkersRender3D(const cv::Mat& img, Cube& d)
	{
		cv::aruco::detectMarkers(img, markerDict, marker_corners, marker_ID);
		cv::aruco::estimatePoseSingleMarkers(marker_corners, 0.1f, Calibrator::intrinsic_matrix, Calibrator::distortion_coeffs, rotat_vec, translat_vec);

		if (marker_ID.size())
		{
			FLAG = marker_ID.size();
			for (int i{}; i < FLAG; ++i)
			{
				cv::Rodrigues(rotat_vec[i], rotation_ogl[i]);

				for (int j{}; j < 3; ++j) translation_ogl[i].at<double>(j) = translat_vec[i][j];

				generate_projection_modelview(Calibrator::intrinsic_matrix, rotation_ogl[i], translation_ogl[i], projection_matx[i], model_view_matx[i]);

				if (marker_ID[i] == 3)
				{
					d.renderCube(projection_matx[i], model_view_matx[i], Types::BASED);
				}
				else
				{
					d.renderCube(projection_matx[i], model_view_matx[i], Types::CRINGE);
				}
			}
		}
		else if (FLAG)
		{
			for (int i{}; i < FLAG; ++i)
			{
				model_view_matx[i] = cv::Mat::eye(4, 4, CV_64F);
				projection_matx[i] = cv::Mat::zeros(4, 4, CV_64F);
			}
			FLAG = 0;
		}
	}

	void findArucoMarkersRender3D(const cv::Mat& img, Planet& d)
	{
		cv::aruco::detectMarkers(img, markerDict, marker_corners, marker_ID);
		cv::aruco::estimatePoseSingleMarkers(marker_corners, 0.1f, Calibrator::intrinsic_matrix, Calibrator::distortion_coeffs, rotat_vec, translat_vec);

		d.use();

		if (marker_ID.size())
		{
			FLAG = marker_ID.size();
			for (int i{}; i < FLAG; ++i)
			{
				cv::Rodrigues(rotat_vec[i], rotation_ogl[i]);

				for (int j{}; j < 3; ++j) translation_ogl[i].at<double>(j) = translat_vec[i][j];

				generate_projection_modelview(Calibrator::intrinsic_matrix, rotation_ogl[i], translation_ogl[i], projection_matx[i], model_view_matx[i]);

				glClear(GL_DEPTH_BUFFER_BIT);

				double *projection_ = convertMatrixType(projection_matx[i]);
				double *modelview_ = convertMatrixType(model_view_matx[i]);
				glm::mat4 modelview = glm::make_mat4(modelview_);
				glm::mat4 projection = glm::make_mat4(projection_);
				glm::mat4 translate = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.05f));
				glm::mat4 pvm = projection * modelview * translate;
				glm::mat4 model = glm::scale(pvm, glm::vec3(0.05f, 0.05f, 0.05f));
				pvm = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));

				glUniformMatrix4fv(glGetUniformLocation(d.ID, "pvm"), 1, GL_FALSE, glm::value_ptr(pvm));
				d.Draw(d);
				delete[] modelview_;
				delete[] projection_;
			}
		}
		else if (FLAG)
		{
			for (int i{}; i < FLAG; ++i)
			{
				model_view_matx[i] = cv::Mat::eye(4, 4, CV_64F);
				projection_matx[i] = cv::Mat::zeros(4, 4, CV_64F);
			}
			FLAG = 0;
		}
	}

private:
	std::vector<int> marker_ID;

	std::vector<std::vector<cv::Point2f>> marker_corners;
	cv::Ptr<cv::aruco::Dictionary> markerDict;

	std::vector<cv::Vec3d> rotat_vec, translat_vec;

	std::vector<cv::Mat> model_view_matx;
	std::vector<cv::Mat> projection_matx;

	std::vector<cv::Mat> rotation_ogl;
	std::vector<cv::Mat> translation_ogl;

	size_t FLAG;

private:

	void generate_projection_modelview(const cv::Mat &calib, const cv::Mat &rotat, const cv::Mat &translat, cv::Mat &projec, cv::Mat &mdlview)
	{
		const double zNear = 0.1;
		const double zFar = 100.0;

		projec.at<double>(0, 0) = calib.at<double>(0, 0) / calib.at<double>(0, 2);
		projec.at<double>(1, 1) = calib.at<double>(1, 1) / calib.at<double>(1, 2);
		projec.at<double>(2, 2) = -(zFar + zNear) / (zFar - zNear);
		projec.at<double>(3, 2) = (-2.0 * zFar * zNear) / (zFar - zNear);
		projec.at<double>(2, 3) = -1;

		mdlview.at<double>(0, 0) = rotat.at<double>(0, 0);
		mdlview.at<double>(1, 0) = rotat.at<double>(1, 0);
		mdlview.at<double>(2, 0) = rotat.at<double>(2, 0);
		mdlview.at<double>(0, 1) = rotat.at<double>(0, 1);
		mdlview.at<double>(1, 1) = rotat.at<double>(1, 1);
		mdlview.at<double>(2, 1) = rotat.at<double>(2, 1);
		mdlview.at<double>(0, 2) = rotat.at<double>(0, 2);
		mdlview.at<double>(1, 2) = rotat.at<double>(1, 2);
		mdlview.at<double>(2, 2) = rotat.at<double>(2, 2);

		mdlview.at<double>(0, 3) = translat.at<double>(0);
		mdlview.at<double>(1, 3) = translat.at<double>(1);
		mdlview.at<double>(2, 3) = translat.at<double>(2);

		static double tmp_coord_array[4][4] = { { 1.,  0.,  0.,  0. },{ 0., -1.,  0.,  0. },{ 0.,  0., -1.,  0. },{ 0.,  0.,  0.,  1. } };
		static cv::Mat change_coord(4, 4, CV_64FC1, tmp_coord_array);
		mdlview = change_coord * mdlview;
		cv::transpose(mdlview, mdlview);
	}
};
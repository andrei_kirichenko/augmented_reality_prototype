#pragma once
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/photo.hpp"
#include "opencv2/calib3d.hpp"
#include <opencv2/aruco.hpp>

class Calibrator
{
protected:
	int img_wid;
	int img_hig;

	cv::Mat intrinsic_matrix;
	cv::Mat distortion_coeffs;

public:
	Calibrator() :
		img_wid(640), img_hig(480),
		intrinsic_matrix((cv::Mat_<double>(3, 3) << 802.6318860752186, 0, 320, 0, 807.8070111121419, 240, 0, 0, 1)),
		distortion_coeffs((cv::Mat_<double>(1, 5) << -0.2156595709873684, 1.484007248088795, 0, 0, -4.103745658466358))
	{}
	
private:
	cv::VideoCapture cap; cv::Mat frame;

public:
	void createArucoMarkers()
	{
		cv::Mat outputMarker;

		cv::Ptr<cv::aruco::Dictionary> markerDict = cv::aruco::getPredefinedDictionary(cv::aruco::PREDEFINED_DICTIONARY_NAME::DICT_4X4_50);

		for (int i{}; i < 50; ++i)
		{
			cv::aruco::drawMarker(markerDict, i, 500, outputMarker);
			std::ostringstream convert;
			std::string imgName = "4x4Marker_";
			convert << imgName << i << ".jpg";
			imwrite(convert.str(), outputMarker);
		}
	}

	void detectAndShowArucoMarker(const float& aruco_sq_dim = 0.1f)
	{
		cap.open(0); if (!cap.isOpened()) { std::cout << "Issue with opening cam for AruCo detection" << std::endl; return; }
		std::vector<std::vector<cv::Point2f>> marker_corners;
		cv::Ptr<cv::aruco::Dictionary> markerDict = cv::aruco::getPredefinedDictionary(cv::aruco::PREDEFINED_DICTIONARY_NAME::DICT_4X4_50);
		cv::namedWindow("Webc", cv::WINDOW_AUTOSIZE);
		std::vector<int> marker_ID;
		std::vector<cv::Vec3d> rotatVec, translatVec;

		while (true)
		{
			if (!cap.read(frame)) break;

			cv::aruco::detectMarkers(frame, markerDict, marker_corners, marker_ID);
			cv::aruco::estimatePoseSingleMarkers(marker_corners, aruco_sq_dim, intrinsic_matrix, distortion_coeffs, rotatVec, translatVec);
			if (marker_ID.size())
			{
				for (int i{}; i < marker_ID.size(); ++i)
				{
					cv::aruco::drawAxis(frame, intrinsic_matrix, distortion_coeffs, rotatVec[i], translatVec[i], 0.1f);
				}
				imshow("Webc", frame);
			}
			imshow("Webc", frame);

			if (cv::waitKey(30) >= 0) break;
		}

		cv::destroyWindow("Webc");
		cap.release();
	}

	void startCalibrationProcess(const int& shots, const int& board_width, const int& board_height, const std::string& file_name = "intrinsics_aruco")
	{
		double delay = 2.f, last_captured_timestamp = 0;
		int board_n = board_width * board_height;
		cv::Size board_sz = cv::Size(board_width, board_height), image_size;

		cap.open(0);

		if (!cap.isOpened()) { std::cout << "\nCouldn't open the camera\n"; return; }

		std::vector<std::vector<cv::Point2f> > image_points;
		std::vector<std::vector<cv::Point3f> > object_points;

		while (image_points.size() < (size_t)shots)
		{
			cv::Mat image0, image;
			cap >> image0;
			image_size = image0.size();
			cv::resize(image0, image, cv::Size(), 0.5f, 0.5f, cv::INTER_LINEAR);

			std::vector<cv::Point2f> corners;
			bool found = cv::findChessboardCorners(image, board_sz, corners);

			double timestamp = static_cast<double>(clock()) / CLOCKS_PER_SEC;
			if (found && timestamp - last_captured_timestamp > delay)
			{
				cv::drawChessboardCorners(image, board_sz, corners, found);
				last_captured_timestamp = timestamp;
				image ^= cv::Scalar::all(255);
				cv::Mat mcorners(corners);
				
				// scale the corner coordinates
				mcorners *= (1.0 / 0.5f);
				image_points.push_back(mcorners);
				object_points.push_back(std::vector<cv::Point3f>());
				std::vector<cv::Point3f>& opts = object_points.back();

				opts.resize(board_n);

				for (int j = 0; j < board_n; j++)
				{
					opts[j] = cv::Point3f(static_cast<float>(j / board_width), static_cast<float>(j % board_width), 0.0f);
				}
				std::cout << "Collected our " << static_cast<uint>(image_points.size()) << " of " << shots << " needed chessboard images\n" << std::endl;
			}
			cv::imshow("Calibration", image);

			if ((cv::waitKey(30) & 255) == 27) return;
		}

		cv::destroyWindow("Calibration");
		cap.release();

		std::cout << "CALIBRATING..." << std::endl;

		double err = cv::calibrateCamera(object_points, image_points, image_size, intrinsic_matrix, distortion_coeffs, cv::noArray(), cv::noArray(), cv::CALIB_ZERO_TANGENT_DIST | cv::CALIB_FIX_PRINCIPAL_POINT);

		std::cout << "Error: " << err << std::endl;

		std::ostringstream convert;
		convert << file_name << ".xml";

		std::cout << " *** DONE! Storing " << convert.str() << " file. *** " << std::endl;

		cv::FileStorage fs(convert.str(), cv::FileStorage::WRITE);

		fs << "image_width" << image_size.width << "image_height" << image_size.height << "camera_matrix" << intrinsic_matrix << "distortion_coefficients" << distortion_coeffs;
		img_wid = image_size.width; img_hig = image_size.height;
		fs.release();
	}

	void readCalibrationData(const std::string& filename)
	{	
		cv::FileStorage fs;
		fs.open(filename, cv::FileStorage::READ);

		if (!fs.isOpened())
		{
			std::cout << "No file called " << filename << " in the folder!" << std::endl;
			return;
		}
		else
		{
			fs["image_width"] >> img_wid; fs["image_height"] >> img_hig;
			fs["camera_matrix"] >> intrinsic_matrix; fs["distortion_coefficients"] >> distortion_coeffs;
			fs.release();
		}

		std::cout << "Image size: " << img_wid << "x" << img_hig << std::endl;
		std::cout << "Camera matrix: \n" << cv::format(intrinsic_matrix, cv::Formatter::FMT_C) << std::endl;
		std::cout << "Distortion coefficients: \n" << cv::format(distortion_coeffs, cv::Formatter::FMT_C) << std::endl;
	}

	void printCurrentData() const 
	{
		std::cout << "Image size: " << img_wid << "x" << img_hig << std::endl;
		std::cout << "Camera matrix: \n" << cv::format(intrinsic_matrix, cv::Formatter::FMT_C) << std::endl;
		std::cout << "Distortion coefficients: \n" << cv::format(distortion_coeffs, cv::Formatter::FMT_C) << std::endl;
	}
};

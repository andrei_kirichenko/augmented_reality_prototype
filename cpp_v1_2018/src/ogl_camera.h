#pragma once

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>

#include <opencv2/highgui.hpp>

#include "shader.h"
#include "mat_to_texture.h"
#include "calibrator.h"

class Backgound : protected Calibrator
{
private:
	static void frame_size_call(GLFWwindow* win, int w, int h) { glViewport(0, 0, w, h); }

	static void process_input(GLFWwindow* win)
	{
		if (glfwGetKey(win, GLFW_KEY_ESCAPE) == GLFW_PRESS) { glfwSetWindowShouldClose(win, true); }
	}

private:
	cv::Mat _img;
	GLFWwindow* _window;

	GLuint _width, _heigth;

public:
	Backgound() :
		_width(Calibrator::img_wid),
		_heigth(Calibrator::img_hig)
	{
		glfwInit();
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

		_window = glfwCreateWindow(_width, _heigth, "OpenGL", nullptr, nullptr);

		if (_window == nullptr) { std::cout << "Failed to create window!" << std::endl; glfwTerminate(); return; }
		glfwMakeContextCurrent(_window);
		glfwSetFramebufferSizeCallback(_window, frame_size_call);

		int gladInitRes = gladLoadGL();
		if (!gladInitRes) { glfwDestroyWindow(_window); glfwTerminate(); return; }

		glEnable(GL_DEPTH_TEST);

		Shader shader_program(_vertexShaderSource, _fragmentShaderSource);
		_prog = shader_program.ID;

		glDeleteShader(shader_program.ID);

		GLfloat vertices[] = {
			1.0f,  1.0f, 0.0f,	1.0f, 1.0f,
			1.0f, -1.0f, 0.0f,	1.0f, 0.0f,
			-1.0f, -1.0f, 0.0f,	0.0f, 0.0f,
			-1.0f,  1.0f, 0.0f,	0.0f, 1.0f
		};

		GLuint indices[] = {
			3, 2, 1,
			1, 0, 3
		};

		glGenVertexArrays(1, &_VAO); glGenBuffers(1, &_VBO); glGenBuffers(1, &_EBO);

		glBindVertexArray(_VAO);
		glBindBuffer(GL_ARRAY_BUFFER, _VBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _EBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
		glEnableVertexAttribArray(0);

		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
		glEnableVertexAttribArray(1);

		_cap.open(0); if (!_cap.isOpened()) { std::cout << "Failed to open camera!" << std::endl; return; }
	}

	void draw()
	{
		_cap >> _img;

		process_input(_window);
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		GLuint tex = matToTexture(_img, GL_LINEAR, GL_LINEAR, GL_REPEAT);
		
		glUseProgram(_prog); glBindVertexArray(_VAO);
		
		glUniform1i(glGetUniformLocation(_prog, "texture1"), 0);
		
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, tex);
		
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);
		
		glDeleteTextures(1, &tex);
	}

	const cv::Mat& getImage() const { return _img; }

	GLFWwindow* getWindow() const { return _window; }

	void swapBuffers() { glfwSwapBuffers(_window); glfwPollEvents(); }

	~Backgound() 
	{ 
		glDeleteVertexArrays(1, &_VAO);
		glDeleteBuffers(1, &_VBO);
		glDeleteBuffers(1, &_EBO);
		_cap.release();
		glfwDestroyWindow(_window);
		glfwTerminate();
	}

private:
	GLuint _prog, _VAO, _VBO, _EBO;

	cv::VideoCapture _cap;

	const char* _vertexShaderSource = "#version 330 core\n"
		"layout (location = 0) in vec3 aPos;\n"
		"layout (location = 1) in vec2 aTexCoord;\n"

		"out vec2 TexCoord;\n"

		"void main()\n"
		"{\n"
		"	gl_Position = vec4(aPos, 1.0f);\n"
		"	TexCoord = vec2(aTexCoord.x, aTexCoord.y);\n"
		"}\0";

	const char* _fragmentShaderSource = "#version 330 core\n"
		"out vec4 gl_FragColor;\n"

		"in vec2 TexCoord;\n"

		"uniform sampler2D texture1;\n"

		"void main()\n"
		"{\n"
		"	gl_FragColor = texture(texture1, TexCoord);\n"
		"}\n\0";
};

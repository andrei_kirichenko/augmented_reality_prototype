#pragma once

#include <opencv2/highgui.hpp>
#include <fstream>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "shader.h"
#include "mat_to_texture.h"

enum Types { TYPE_1, TYPE_2 };

static double *convertMatrixType(const cv::Mat &mtx)
{
	int size = mtx.rows * mtx.cols;
	double *tmp = new double[size];
	for (int i{}; i < mtx.rows; ++i)
	{
		for (int j{}; j < mtx.cols; ++j)
		{
			tmp[i * mtx.cols + j] = mtx.at<double>(i, j);
		}
	}
	return tmp;
}

class Cube
{
public:
	Cube()
	{
		std::fstream file("cube.vert");
		std::string src_vert(std::istreambuf_iterator<char>(file), (std::istreambuf_iterator<char>()));
		file.close();

		file.open("cube.frag");
		std::string src_frag(std::istreambuf_iterator<char>(file), (std::istreambuf_iterator<char>()));
		file.close();

		Shader shader_program_cube(src_vert.c_str(), src_frag.c_str());
		_prog = shader_program_cube.ID;
		generateCube(_VAO, _VBO);

		cv::Mat type_1 = cv::imread("a.jpg", cv::IMREAD_ANYCOLOR);
		first_texture = matToTexture(type_1, GL_LINEAR, GL_LINEAR, GL_REPEAT);

		cv::Mat type_2 = cv::imread("b.jpg", cv::IMREAD_ANYCOLOR);
		second_texture = matToTexture(type_2, GL_LINEAR, GL_LINEAR, GL_REPEAT);
	}
	
	void renderCube(cv::Mat& projection_matx, cv::Mat& model_view_matx, Types type)
	{
		glClear(GL_DEPTH_BUFFER_BIT);

		double *projection_ = convertMatrixType(projection_matx);
		double *modelview_ = convertMatrixType(model_view_matx);
		glm::mat4 modelview = glm::make_mat4(modelview_);
		glm::mat4 projection = glm::make_mat4(projection_);
		glm::mat4 translate = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.05f));
		glm::mat4 pvm = projection * modelview * translate;
		glUseProgram(_prog);
		glBindVertexArray(_VAO);
		glUniform1i(glGetUniformLocation(_prog, "texture1"), 0);
		glActiveTexture(GL_TEXTURE0);

		if (type == Types::TYPE_2) { glBindTexture(GL_TEXTURE_2D, second_texture); }
		else if (type == Types::TYPE_1) { glBindTexture(GL_TEXTURE_2D, first_texture); }

		glUniformMatrix4fv(glGetUniformLocation(_prog, "pvm"), 1, GL_FALSE, glm::value_ptr(pvm));
		glDrawArrays(GL_TRIANGLES, 0, 36);
		delete[] modelview_;
		delete[] projection_;
	}

	~Cube()
	{
		glDeleteVertexArrays(1, &_VAO);
		glDeleteBuffers(1, &_VBO);
	}

private:
	void generateCube(GLuint& vao, GLuint& vbo)
	{
		GLfloat vertices2[] = {
			-0.05f, -0.05f, -0.05f,  0.0f, 0.0f,
			0.05f, -0.05f, -0.05f,  1.0f, 0.0f,
			0.05f,  0.05f, -0.05f,  1.0f, 1.0f,
			0.05f,  0.05f, -0.05f,  1.0f, 1.0f,
			-0.05f,  0.05f, -0.05f,  0.0f, 1.0f,
			-0.05f, -0.05f, -0.05f,  0.0f, 0.0f,

			-0.05f, -0.05f,  0.05f,  0.0f, 0.0f,
			0.05f, -0.05f,  0.05f,  1.0f, 0.0f,
			0.05f,  0.05f,  0.05f,  1.0f, 1.0f,
			0.05f,  0.05f,  0.05f,  1.0f, 1.0f,
			-0.05f,  0.05f,  0.05f,  0.0f, 1.0f,
			-0.05f, -0.05f,  0.05f,  0.0f, 0.0f,

			-0.05f,  0.05f,  0.05f,  1.0f, 0.0f,
			-0.05f,  0.05f, -0.05f,  1.0f, 1.0f,
			-0.05f, -0.05f, -0.05f,  0.0f, 1.0f,
			-0.05f, -0.05f, -0.05f,  0.0f, 1.0f,
			-0.05f, -0.05f,  0.05f,  0.0f, 0.0f,
			-0.05f,  0.05f,  0.05f,  1.0f, 0.0f,

			0.05f,  0.05f,  0.05f,  1.0f, 0.0f,
			0.05f,  0.05f, -0.05f,  1.0f, 1.0f,
			0.05f, -0.05f, -0.05f,  0.0f, 1.0f,
			0.05f, -0.05f, -0.05f,  0.0f, 1.0f,
			0.05f, -0.05f,  0.05f,  0.0f, 0.0f,
			0.05f,  0.05f,  0.05f,  1.0f, 0.0f,

			-0.05f, -0.05f, -0.05f,  0.0f, 1.0f,
			0.05f, -0.05f, -0.05f,  1.0f, 1.0f,
			0.05f, -0.05f,  0.05f,  1.0f, 0.0f,
			0.05f, -0.05f,  0.05f,  1.0f, 0.0f,
			-0.05f, -0.05f,  0.05f,  0.0f, 0.0f,
			-0.05f, -0.05f, -0.05f,  0.0f, 1.0f,

			-0.05f,  0.05f, -0.05f,  0.0f, 1.0f,
			0.05f,  0.05f, -0.05f,  1.0f, 1.0f,
			0.05f,  0.05f,  0.05f,  1.0f, 0.0f,
			0.05f,  0.05f,  0.05f,  1.0f, 0.0f,
			-0.05f,  0.05f,  0.05f,  0.0f, 0.0f,
			-0.05f,  0.05f, -0.05f,  0.0f, 1.0f
		};

		glGenVertexArrays(1, &vao);
		glGenBuffers(1, &vbo);
		glBindVertexArray(vao);

		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices2), vertices2, GL_STATIC_DRAW);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
		glEnableVertexAttribArray(1);
	}

private:
	GLuint _prog, _VBO, _VAO;

	GLuint first_texture;
	GLuint second_texture;
};
#pragma once
#include "shader.h"
#include "model.h"

const char* _vertexShaderSource_planet = "#version 330 core\n"
"layout(location = 0) in vec3 aPos;\n"
"layout(location = 1) in vec3 aNormal;\n"
"layout(location = 2) in vec2 aTexCoords;\n"

"out vec2 TexCoords;\n"

"uniform mat4 pvm;\n"

"void main()\n"
"{\n"
"	TexCoords = aTexCoords;\n"
"	gl_Position = pvm * vec4(aPos, 1.0);\n"
"}\n";

const char* _fragmentShaderSource_planet = "#version 330 core\n"
"out vec4 FragColor;\n"

"in vec2 TexCoords;\n"

"uniform sampler2D texture_diffuse1;\n"

"void main()\n"
"{\n"
"	FragColor = texture(texture_diffuse1, TexCoords);\n"
"}\n";

struct Planet: public Shader, public Model 
{ 
	Planet() : Shader(_vertexShaderSource_planet, _fragmentShaderSource_planet), Model("saturn/saturn.obj") {}
};

#pragma once

#include <glad/glad.h>

#include <opencv2/opencv.hpp>
#include <iostream>

// cv::Mat img_2 = cv::imread("3.jpg", cv::IMREAD_ANYCOLOR);
// GLuint texture2 = matToTexture(img_2, GL_LINEAR, GL_LINEAR, GL_REPEAT);

GLuint matToTexture(cv::Mat &mat, GLenum minFilter, GLenum magFilter, GLenum wrapFilter)
{
	// rotate the pic
	cv::Mat dst; 
	cv::flip(mat, dst, 0);

	GLuint textureID;
	glGenTextures(1, &textureID);
	glBindTexture(GL_TEXTURE_2D, textureID);

	if (magFilter == GL_LINEAR_MIPMAP_LINEAR || 
		magFilter == GL_LINEAR_MIPMAP_NEAREST || 
		magFilter == GL_NEAREST_MIPMAP_LINEAR || 
		magFilter == GL_NEAREST_MIPMAP_NEAREST)
	{
		std::cout << "You can't use MIPMAPs for magnification - setting filter to GL_LINEAR" << std::endl; magFilter = GL_LINEAR;
	}

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrapFilter);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrapFilter);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, minFilter);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, magFilter);

	GLenum inputColourFormat = GL_BGR;

	// Create the texture
	glTexImage2D(GL_TEXTURE_2D,     // Type of texture
		0,
		GL_RGB,						// Internal colour format to convert to
		dst.cols,
		dst.rows,
		0,							// Border width in pixels (can either be 1 or 0)
		inputColourFormat,			// Input image format (i.e. GL_RGB, GL_RGBA, GL_BGR etc.)
		GL_UNSIGNED_BYTE,			// Image data type
		dst.ptr());					// The actual image data itself

	// If we're using mipmaps then generate them. OpenGL 3.0 or higher
	if (minFilter == GL_LINEAR_MIPMAP_LINEAR ||	
		minFilter == GL_LINEAR_MIPMAP_NEAREST || 
		minFilter == GL_NEAREST_MIPMAP_LINEAR || 
		minFilter == GL_NEAREST_MIPMAP_NEAREST) 
	{ 
		glGenerateMipmap(GL_TEXTURE_2D); 
	}

	return textureID;
}
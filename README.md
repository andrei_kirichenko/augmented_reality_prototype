# augmented_reality_prototype

## draft projects/src files

Initially was developed to learn C++, OpenCV and OpenGL. Later a Python prototype was created to test simple occlusion handling solution.

![v1](demo_cpp.png?raw=true)
![v2](demo_py.PNG?raw=true)

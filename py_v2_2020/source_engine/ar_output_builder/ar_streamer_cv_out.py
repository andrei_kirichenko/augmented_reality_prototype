import cv2
import glfw
import numpy as np
from OpenGL.GL import *
from PIL import Image

from camera_control.main_camera import Camera
from occlusion_handler.occlusion_handler import OcclusionHandler
from src.marker_based_detection.aruco_detector import ArucoProcessor
from src.source_engine.gl_objects.textured_cube_object_cv_out import TexturedCube


class EngineAR(Camera, ArucoProcessor, OcclusionHandler):
    def __init__(self, wid=640, hei=480):
        Camera.__init__(self)
        ArucoProcessor.__init__(self)
        OcclusionHandler.__init__(self)

        self.width = wid
        self.height = hei

        if not glfw.init():
            raise Exception(f'GLFW is not initialised!')

        glfw.window_hint(glfw.CONTEXT_VERSION_MAJOR, 3)
        glfw.window_hint(glfw.CONTEXT_VERSION_MAJOR, 3)

        glfw.window_hint(glfw.VISIBLE, glfw.FALSE)

        self.window = glfw.create_window(self.width, self.height, "EngineAR", None, None)

        if not self.window:
            glfw.terminate()
            raise Exception(f'Window is not created!')

        glfw.make_context_current(self.window)

        glEnable(GL_DEPTH_TEST)
        glEnable(GL_LINE_SMOOTH)

        glClearColor(0.0, 0.0, 0.0, 0.0)

    def __extract_frame_from_opengl_context(self):
        glReadBuffer(GL_FRONT)
        screenshot_pixels = glReadPixels(0, 0, self.width, self.height, GL_RGBA, GL_UNSIGNED_BYTE)
        resulting_image_from_ogl = np.array(Image.frombytes('RGBA', (self.width, self.height), screenshot_pixels))
        return cv2.flip(cv2.cvtColor(resulting_image_from_ogl, cv2.COLOR_RGBA2BGRA), 0)

    @staticmethod
    def merge_alpha_channels(background, overlay, x=0, y=0):
        background_height = background.shape[0]
        background_width = background.shape[1]

        h, w = overlay.shape[0], overlay.shape[1]

        if x + w > background_width:
            w = background_width - x
            overlay = overlay[:, :w]

        if y + h > background_height:
            h = background_height - y
            overlay = overlay[:h]

        if overlay.shape[2] < 4:
            overlay = np.concatenate(
                [overlay, np.ones((overlay.shape[0], overlay.shape[1], 1), dtype=overlay.dtype) * 255], axis=2,
            )

        overlay_image = overlay[..., :3]
        mask = overlay[..., 3:] / 255.0

        background[y:y + h, x:x + w] = (1.0 - mask) * background[y:y + h, x:x + w] + mask * overlay_image

        return background

    def visualise_scene(self, obj=None):
        while not glfw.window_should_close(self.window):
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
            glfw.poll_events()

            frame = self.get_current_frame_from_web_camera()

            # BEGIN SEARCH MARKERS & DRAW OBJECTS
            r_matrix, t_matrix = self.get_coords_of_aruco_marker(frame)
            if t_matrix is not None and r_matrix is not None and obj is not None:
                obj.render(ArucoProcessor.generate_pmv_matrix(calibration_mat=self.camera_matrix,
                                                              rotation_vec=r_matrix,
                                                              translation_vec=t_matrix))
            # END

            # BEGIN EXTRACT FRAME FROM GL CONTEXT
            resulting_image_from_ogl = self.__extract_frame_from_opengl_context()
            # END

            # BEGIN OCCLUSION HANDLING ACTIONS
            resulting_image_from_ogl = self.handle(frame, resulting_image_from_ogl)
            # END

            # BEGIN ALPHA COMPOSITION OF IMAGES FROM CAM & GL CONTEXT
            result = self.merge_alpha_channels(frame, resulting_image_from_ogl)
            # END

            cv2.imshow("EngineAR", result)

            if cv2.waitKey(1) & 0xFF == 27:
                glfw.set_window_should_close(self.window, True)

            glfw.swap_buffers(self.window)


if __name__ == "__main__":
    ear = EngineAR()
    textured_cube = TexturedCube()
    ear.visualise_scene(textured_cube)

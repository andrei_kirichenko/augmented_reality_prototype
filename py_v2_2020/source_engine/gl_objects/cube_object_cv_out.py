import os

import pyrr
from OpenGL.GL import *
import OpenGL.GL.shaders
import numpy as np


class Cube:
    def __init__(self):
        self.shader = None
        self.VBO = glGenBuffers(1)
        self.EBO = glGenBuffers(1)
        self.color = 1
        self.position = 0

        cube = [-0.05, -0.05, 0.05, 0.0, 0.0, 0.0,
                0.05, -0.05, 0.05, 0.0, 1.0, 0.0,
                0.05, 0.05, 0.05, 0.0, 1.0, 0.0,
                -0.05, 0.05, 0.05, 1.0, 1.0, 1.0,

                -0.05, -0.05, -0.05, 1.0, 0.0, 0.0,
                0.05, -0.05, -0.05, 0.0, 1.0, 0.0,
                0.05, 0.05, -0.05, 0.0, 1.0, 1.0,
                -0.05, 0.05, -0.05, 1.0, 1.0, 1.0]

        self.cube = np.array(cube, dtype=np.float32)

        indices = [0, 1, 2, 2, 3, 0,
                   4, 5, 6, 6, 7, 4,
                   4, 5, 1, 1, 0, 4,
                   6, 7, 3, 3, 2, 6,
                   5, 6, 2, 2, 1, 5,
                   7, 4, 0, 0, 3, 7]

        self.indices = np.array(indices, dtype=np.uint32)

        dir_path = os.path.dirname(os.path.realpath(__file__))

        with open(dir_path + '\\resources\\cube_obj.vs', 'r') as filea:
            with open(dir_path + '\\resources\\cube_obj.fs', 'r') as fileb:
                self.VERTEX_SHADER = filea.read()
                self.FRAGMENT_SHADER = fileb.read()

        self.shader = OpenGL.GL.shaders.compileProgram(
            OpenGL.GL.shaders.compileShader(self.VERTEX_SHADER, GL_VERTEX_SHADER),
            OpenGL.GL.shaders.compileShader(self.FRAGMENT_SHADER, GL_FRAGMENT_SHADER)
        )

        glBindBuffer(GL_ARRAY_BUFFER, self.VBO)
        glBufferData(GL_ARRAY_BUFFER, self.cube.itemsize * len(self.cube), self.cube, GL_STATIC_DRAW)

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, self.EBO)
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, self.indices.itemsize * len(self.indices), self.indices, GL_STATIC_DRAW)

        glBindAttribLocation(self.shader, self.position, "position")
        glBindAttribLocation(self.shader, self.color, "color")

        glVertexAttribPointer(self.position, 3, GL_FLOAT, GL_FALSE, 24, ctypes.c_void_p(0))
        glEnableVertexAttribArray(self.position)

        glVertexAttribPointer(self.color, 3, GL_FLOAT, GL_FALSE, 24, ctypes.c_void_p(12))
        glEnableVertexAttribArray(self.color)

    def render(self, pvm):
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        glUseProgram(self.shader)

        offset_mat = pyrr.matrix44.create_from_translation(np.array([0.0, 0.0, 0.05], dtype=np.float64))
        pvm = np.matmul(offset_mat, pvm)

        glUniformMatrix4fv(glGetUniformLocation(self.shader, "pvm"), 1, GL_FALSE, pvm)

        glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, None)

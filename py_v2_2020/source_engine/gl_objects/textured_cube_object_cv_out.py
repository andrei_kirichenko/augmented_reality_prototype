import os

import pyrr
from OpenGL.GL import *
import OpenGL.GL.shaders
import numpy as np
import cv2


class TexturedCube:
    def __init__(self):
        self.shader = None

        self.VBO = glGenBuffers(1)
        self.EBO = glGenBuffers(1)
        self.TBO = glGenTextures(1)

        self.position = 0
        self.color = 1
        self.tex_coords = 2

        cube = [-0.05, -0.05, 0.05, 1.0, 0.0, 0.0, 0.0, 0.0,
                0.05, -0.05, 0.05, 0.0, 1.0, 0.0, 1.0, 0.0,
                0.05, 0.05, 0.05, 0.0, 0.0, 1.0, 1.0, 1.0,
                -0.05, 0.05, 0.05, 1.0, 1.0, 1.0, 0.0, 1.0,

                -0.05, -0.05, -0.05, 1.0, 0.0, 0.0, 0.0, 0.0,
                0.05, -0.05, -0.05, 0.0, 1.0, 0.0, 1.0, 0.0,
                0.05, 0.05, -0.05, 0.0, 0.0, 1.0, 1.0, 1.0,
                -0.05, 0.05, -0.05, 1.0, 1.0, 1.0, 0.0, 1.0,

                0.05, -0.05, -0.05, 1.0, 0.0, 0.0, 0.0, 0.0,
                0.05, 0.05, -0.05, 0.0, 1.0, 0.0, 1.0, 0.0,
                0.05, 0.05, 0.05, 0.0, 0.0, 1.0, 1.0, 1.0,
                0.05, -0.05, 0.05, 1.0, 1.0, 1.0, 0.0, 1.0,

                -0.05, 0.05, -0.05, 1.0, 0.0, 0.0, 0.0, 0.0,
                -0.05, -0.05, -0.05, 0.0, 1.0, 0.0, 1.0, 0.0,
                -0.05, -0.05, 0.05, 0.0, 0.0, 1.0, 1.0, 1.0,
                -0.05, 0.05, 0.05, 1.0, 1.0, 1.0, 0.0, 1.0,

                -0.05, -0.05, -0.05, 1.0, 0.0, 0.0, 0.0, 0.0,
                0.05, -0.05, -0.05, 0.0, 1.0, 0.0, 1.0, 0.0,
                0.05, -0.05, 0.05, 0.0, 0.0, 1.0, 1.0, 1.0,
                -0.05, -0.05, 0.05, 1.0, 1.0, 1.0, 0.0, 1.0,

                0.05, 0.05, -0.05, 1.0, 0.0, 0.0, 0.0, 0.0,
                -0.05, 0.05, -0.05, 0.0, 1.0, 0.0, 1.0, 0.0,
                -0.05, 0.05, 0.05, 0.0, 0.0, 1.0, 1.0, 1.0,
                0.05, 0.05, 0.05, 1.0, 1.0, 1.0, 0.0, 1.0]

        self.vertices = np.array(cube, dtype=np.float32)

        indices = [0, 1, 2, 2, 3, 0,
                   4, 5, 6, 6, 7, 4,
                   8, 9, 10, 10, 11, 8,
                   12, 13, 14, 14, 15, 12,
                   16, 17, 18, 18, 19, 16,
                   20, 21, 22, 22, 23, 20]

        self.indices = np.array(indices, dtype=np.uint32)

        dir_path = os.path.dirname(os.path.realpath(__file__))

        with open(dir_path + '\\resources\\textured_cube_obj.vs', 'r') as filea:
            with open(dir_path + '\\resources\\textured_cube_obj.fs', 'r') as fileb:
                self.VERTEX_SHADER = filea.read()
                self.FRAGMENT_SHADER = fileb.read()

        self.shader = OpenGL.GL.shaders.compileProgram(
            OpenGL.GL.shaders.compileShader(self.VERTEX_SHADER, GL_VERTEX_SHADER),
            OpenGL.GL.shaders.compileShader(self.FRAGMENT_SHADER, GL_FRAGMENT_SHADER)
        )

        glBindAttribLocation(self.shader, self.position, 'position')
        glBindAttribLocation(self.shader, self.color, 'color')
        glBindAttribLocation(self.shader, self.tex_coords, 'InTexCoords')

        glBindBuffer(GL_ARRAY_BUFFER, self.VBO)
        glBufferData(GL_ARRAY_BUFFER, self.vertices.itemsize * len(self.vertices), self.vertices, GL_STATIC_DRAW)

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, self.EBO)
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, self.indices.itemsize * len(self.indices), self.indices, GL_STATIC_DRAW)

        glVertexAttribPointer(self.position, 3, GL_FLOAT, GL_FALSE, self.vertices.itemsize * 8, ctypes.c_void_p(0))
        glEnableVertexAttribArray(self.position)

        glVertexAttribPointer(self.color, 3, GL_FLOAT, GL_FALSE, self.vertices.itemsize * 8, ctypes.c_void_p(12))
        glEnableVertexAttribArray(self.color)

        glVertexAttribPointer(self.tex_coords, 2, GL_FLOAT, GL_FALSE, self.vertices.itemsize * 8, ctypes.c_void_p(24))
        glEnableVertexAttribArray(self.tex_coords)

        glBindTexture(GL_TEXTURE_2D, self.TBO)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)

        dir_path = os.path.dirname(os.path.realpath(__file__))

        image = cv2.cvtColor(cv2.imread(dir_path + '\\resources\\logo.png', cv2.IMREAD_COLOR), cv2.COLOR_BGR2RGB)
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, image.shape[1], image.shape[0], 0, GL_RGB, GL_UNSIGNED_BYTE, image)
        glEnable(GL_TEXTURE_2D)

    def render(self, pvm):
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        glUseProgram(self.shader)

        offset_mat = pyrr.matrix44.create_from_translation(np.array([0.0, 0.0, 0.05], dtype=np.float64))
        pvm = np.matmul(offset_mat, pvm)

        glUniformMatrix4fv(glGetUniformLocation(self.shader, "pvm"), 1, GL_FALSE, pvm)

        glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, None)

#version 330

in vec3 position;
in vec2 InTexCoords;

out vec2 OutTexCoords;

void main() {
    gl_Position = vec4(position, 1.0);
    OutTexCoords = InTexCoords;
}

import cv2

"""
Класс камеры, предназначенный для считывания потока изображений с источника видео.
"""


class Camera:
    # Инициализация камеры, в случае если камера на устройстве не найдена - выбрасывается исключение
    def __init__(self):
        self.__cap = cv2.VideoCapture(0)

        if not self.__cap.isOpened():
            raise Exception(f"Camera is not connected!")

    # Функция получения изображения с потока видео
    def get_current_frame_from_web_camera(self):
        return self.__cap.read()[1]

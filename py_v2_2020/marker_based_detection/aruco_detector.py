import math

import cv2
import cv2.aruco as aruco
import numpy as np

from camera_calibration.camera_parameters import CalibrationData
from camera_control.main_camera import Camera


class ArucoProcessor(CalibrationData):
    def __init__(self):
        CalibrationData.__init__(self)
        self.scannable_marker_dict = aruco.Dictionary_get(aruco.DICT_4X4_50)

    def get_coords_of_aruco_marker(self, frame):
        corners, ids, _ = aruco.detectMarkers(frame, self.scannable_marker_dict)

        if ids is not None:
            r_vectors, t_vectors, _ = aruco.estimatePoseSingleMarkers(corners,
                                                                      0.1,
                                                                      self.get_calibration_data()[0],
                                                                      self.get_calibration_data()[1])

            return r_vectors[0], t_vectors[0]

        return None, None

    @staticmethod
    def generate_pmv_matrix(calibration_mat, rotation_vec, translation_vec):
        zNear = 0.1
        zFar = 100.0

        # Преобразование вектора поворота в матрицу ротации с помощью формулы поворота Родрига
        rotation_mat = cv2.Rodrigues(np.array(rotation_vec), None)[0]

        # Сборка проекционной матрицы
        projection_mat = np.zeros((4, 4), dtype=np.float32)

        projection_mat[0][0] = calibration_mat[0][0] / calibration_mat[0][2]
        projection_mat[1][1] = calibration_mat[1][1] / calibration_mat[1][2]
        projection_mat[2][2] = -(zFar + zNear) / (zFar - zNear)
        projection_mat[3][2] = (-2.0 * zFar * zNear) / (zFar - zNear)
        projection_mat[2][3] = -1.0

        # Сборка матрицы модели/вида
        model_view_mat = np.eye(4, dtype=np.float32)

        model_view_mat[0][0] = rotation_mat[0][0]
        model_view_mat[1][0] = rotation_mat[1][0]
        model_view_mat[2][0] = rotation_mat[2][0]

        model_view_mat[0][1] = rotation_mat[0][1]
        model_view_mat[1][1] = rotation_mat[1][1]
        model_view_mat[2][1] = rotation_mat[2][1]

        model_view_mat[0][2] = rotation_mat[0][2]
        model_view_mat[1][2] = rotation_mat[1][2]
        model_view_mat[2][2] = rotation_mat[2][2]

        model_view_mat[0][3] = translation_vec[0][0]
        model_view_mat[1][3] = translation_vec[0][1]
        model_view_mat[2][3] = translation_vec[0][2]

        # Инвертирование координат для фиксации направлений осей по отношению к камере
        inverse_matrix = np.array([[1.0, 1.0, 1.0, 1.0],
                                   [-1.0, -1.0, -1.0, -1.0],
                                   [-1.0, -1.0, -1.0, -1.0],
                                   [1.0, 1.0, 1.0, 1.0]], dtype=np.float32)
        model_view_mat = model_view_mat * inverse_matrix

        # Перемножение матриц в правильном порядке
        return np.matmul(np.transpose(model_view_mat), projection_mat)


if __name__ == '__main__':
    ap = ArucoProcessor()
    cam = Camera()

    while True:
        frame = cam.get_current_frame_from_web_camera()
        corners, ids, rejectedImagePoints = aruco.detectMarkers(frame, ap.scannable_marker_dict)

        if ids is not None:
            for i in range(len(ids)):
                print('\n\nMarker ID: {}'.format(ids[i]))

                frame = aruco.drawDetectedMarkers(frame, corners, borderColor=(0, 0, 255))

                r_vectors, t_vectors, _ = aruco.estimatePoseSingleMarkers(corners,
                                                                          0.1,
                                                                          ap.get_calibration_data()[0],
                                                                          ap.get_calibration_data()[1])

                aruco.drawAxis(frame, ap.get_calibration_data()[0], ap.get_calibration_data()[1], r_vectors,
                               t_vectors, 0.05)

                cv2.putText(frame,
                            f"Distance: {(t_vectors[i][0][2] * 100):.2f} cm ",
                            (25, 25), cv2.FONT_HERSHEY_PLAIN, 1.0, (0, 0, 244), 2)

                cv2.putText(frame,
                            f"Angle: {(r_vectors[i][0][2] / math.pi * 180):.2f} deg",
                            (25, 40), cv2.FONT_HERSHEY_PLAIN, 1.0, (0, 0, 244), 2)

        cv2.imshow('ArucoProjector', frame)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

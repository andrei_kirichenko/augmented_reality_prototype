import cv2


class OcclusionHandler:
    def __init__(self):
        self.static_background = None
        self.test = True

    @staticmethod
    def get_ogl_frame_mask(img):
        gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        gray_img = cv2.GaussianBlur(gray_img, (5, 5), 2)
        thresh_img = cv2.threshold(gray_img, 1, 255, cv2.THRESH_BINARY)[1]
        thresh_img = cv2.erode(thresh_img, None, iterations=2)
        thresh_img = cv2.dilate(thresh_img, None, iterations=2)
        return thresh_img

    def handle(self, cv_img, gl_img):
        if cv_img is None or gl_img is None:
            raise Exception('EXCEPTION::Occlusion handling::handle()::received empty data')

        gl_mask = self.get_ogl_frame_mask(gl_img)

        gray_copy = cv2.cvtColor(cv_img, cv2.COLOR_BGR2GRAY)
        gray_copy = cv2.GaussianBlur(gray_copy, (21, 21), 0)

        if self.static_background is None:
            self.static_background = gray_copy
            self.test = False

        diff_frame = cv2.absdiff(self.static_background, gray_copy)

        thresh_frame = cv2.threshold(diff_frame, 25, 255, cv2.THRESH_BINARY)[1]
        thresh_frame = cv2.erode(thresh_frame, None, iterations=2)
        thresh_frame = cv2.dilate(thresh_frame, None, iterations=2)
        cv_mask = cv2.bitwise_not(thresh_frame)
        occlusion_mask = cv2.bitwise_and(gl_mask, cv_mask)

        return cv2.bitwise_and(
            gl_img,
            gl_img,
            mask=occlusion_mask
        )

import os

import cv2

"""
Класс, читающий данные калибровки для их дальнейшего использования в разрабатываемом приложении дополненной реальности 
"""


class CalibrationData:
    # Инициализация калибровочной матрицы камеры и её данных об искажении объектива
    def __init__(self):
        dir_path = os.path.dirname(os.path.realpath(__file__))
        s = cv2.FileStorage(dir_path + "\\calib_res_chessboard_default.yaml",
                            cv2.FILE_STORAGE_READ)

        if not s.isOpened():
            raise Exception(f"Can't find file!")

        self.camera_matrix = s.getNode("camera_matrix").mat()
        self.distortion_coefficient = s.getNode("distortion_coefficient").mat()

        if self.camera_matrix is None or self.distortion_coefficient is None:
            raise Exception("Empty matrices!")

    # 
    def get_calibration_data(self):
        if self.camera_matrix is None or self.distortion_coefficient is None:
            raise Exception(f"Can't return empty values!")
        return self.camera_matrix, self.distortion_coefficient

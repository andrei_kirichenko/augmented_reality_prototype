# Исходный код файла camera_calibrator.py

import time

import cv2
import numpy as np

from src.camera_control.main_camera import Camera

"""
Класс таймер, предназначенный для временных событий
"""


class Timer:
    def __init__(self):
        self._start_time = None

    # Старт отсчета
    def start(self):
        if self._start_time is not None:
            raise Exception(f"Timer is already running!")

        self._start_time = time.perf_counter()

    # Конец отсчета
    def stop(self):
        if self._start_time is None:
            raise Exception(f"Timer is not running!")

        self._start_time = None

    # Итоговый интервал времени
    def check_elapsed_seconds(self):
        return time.perf_counter() - self._start_time


"""
Класс калибровщик, используется для калибровки монокамеры и сохранения результатов
"""


class CameraCalibrator(Camera):
    def __init__(self):
        Camera.__init__(self)

    # Функция калибровки и сохранения параметров камеры
    # Калибровка происходит по калибровочному шаблону шахматной доски
    # В качестве данных на вход выступают:
    # * колличество рисунков для калибровки (по умолчанию = 9)
    # * колличество углов шахматной доски в ширину (по умолчанию = 7)
    # * колличество углов шачматной доски в высоту (по умолчанию = 10)
    def calibrate_camera_with_chessboard_pattern(self,
                                                 frames_to_acquire: int = 9,
                                                 width: int = 7,
                                                 height: int = 10):
        # Сборка данных, объявление переменных и массивов
        chessboard_dim = (width, height)
        obj_points = []
        img_points = []

        obj_points_container = np.zeros(
            (1, chessboard_dim[0] * chessboard_dim[1], 3),
            np.float32)

        obj_points_container[0, :, :2] = np.mgrid[
                                         0:chessboard_dim[0],
                                         0:chessboard_dim[1]
                                         ].T.reshape(-1, 2)

        calibration_timer = Timer()
        calibration_timer.start()

        shots = 0
        frame_dim = None

        # Поиск углов шахматной доски
        # Если найден - добавляется в коллекции, ждет одну секунду и продолжает
        while True:
            frame = self.get_current_frame_from_web_camera()

            if calibration_timer.check_elapsed_seconds() >= 3:
                gray = cv2.cvtColor(src=frame,
                                    code=cv2.COLOR_BGR2GRAY)

                ret, corners = cv2.findChessboardCorners(gray,
                                                         chessboard_dim,
                                                         flags=cv2.CALIB_CB_ADAPTIVE_THRESH +
                                                               cv2.CALIB_CB_FAST_CHECK +
                                                               cv2.CALIB_CB_NORMALIZE_IMAGE)

                if ret:
                    obj_points.append(obj_points_container)

                    img_points.append(corners)

                    time.sleep(1)
                    calibration_timer.stop()
                    calibration_timer.start()
                    shots += 1
                    print(f"{shots} / {frames_to_acquire} shots taken.")

            cv2.imshow("frame", frame)

            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

            if shots == frames_to_acquire:
                frame_dim = frame.shape[:2]
                break

        calibration_timer.stop()

        if len(obj_points) == 0 or len(img_points) == 0:
            raise Exception(f"Nothing to process!")

        start = time.time()
        # Старт калибровки
        # Возвращает:
        # * ошибку репроецирования
        # * калибровочную матрицу камеры
        # * коэффициенты дисторсии
        ret, camera_matrix, distortion_coefficient, _, _ = \
            cv2.calibrateCamera(obj_points,
                                img_points,
                                frame_dim,
                                cameraMatrix=None,
                                distCoeffs=None)

        end = time.time()
        print(f"Callibration time: {end - start:0.5f} sec.")
        print(f"Accuracy value : {ret:0.2f} %")

        # Сохранение результатов калибровки
        s = cv2.FileStorage(f"calib_res_chessboard_logitech_c270.yaml", cv2.FILE_STORAGE_WRITE)

        s.write("camera_matrix", camera_matrix)
        s.write("distortion_coefficient", distortion_coefficient)

        s.release()


if __name__ == '__main__':
    c = CameraCalibrator()
    c.calibrate_camera_with_chessboard_pattern(frames_to_acquire=50)

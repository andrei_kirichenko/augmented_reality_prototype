import cv2
import glfw
import numpy


from source_engine.ar_output_builder.ar_streamer_cv_out import EngineAR
from source_engine.gl_objects.textured_cube_object_cv_out import TexturedCube


def main():
    print(cv2.__version__)
    print(glfw.__version__)
    print(numpy.__version__)

    ear = EngineAR()
    textured_cube = TexturedCube()
    ear.visualise_scene(textured_cube)


if __name__ == '__main__':
    main()
